# PROJECT HOME INFRA

initial setup I used Rocky linux as base, however I had some issues with the firewall as it's not using iptables anymore, which is absolutely fine, just have some bugs when bringing down network stays in firewalld rules and when recreated on same name, the firewalld exits with error, which causing issues when server restarted or whole service stack rebuilt.

i could use podman, but for now i just sticked with docker and just booted up a server with ubuntu for now, when the setup is more tested. I will give it a try with podman-compose 


## REVERSE PROXY_BASE

1. Copy .env.sample to .env
2. Edit variables to your need IP address , email address. IP adress should be LAN IP
3. start up swag once with `sudo docker-compose up swag`, let it run to populate directory, then when its says that token not valid stop it with CTRL+C 
4. Go into the swag config/dns-conf/cloudflare.ini  and edit email address and domain API token
5. Now when we start it up it should get a new certificate and should say Server Ready. Now we have valid cert on a NGINX reverse proxy.
6. Adding all service to reverse proxy with copying .sample of all the service mentioned in the docker-compose to same filename without .sample

to restart swag to apply newly added proxy-confs or change settings do `docker-compose restart swag`. To bring everything up `docker-compose up -d` -d means daemon. In case there was fundamental changes some container needs recreation then its better just add `docker-compose up -d --force-recreation`.
In case you want to update versions `docker-compose pull && docker-compose up -d`

## Nextcloud and collabora

7. Nextcloud installation, then  edit config.php as mentioned in the comments of nextcloud.subdomain.conf and changing memcache to redis https://docs.nextcloud.com/server/19/admin_manual/configuration_server/caching_configuration.html#id2
8. Add nextcloud office and set to your domain address of collabora.
9. Enjoy you have a nextcloud collabora combination, with Google Office like collaboration features.

## Bitwarden /vaultwarden

Valutwarden is the RUST reimplamantation of Bitwarden without the massive security layers,separations and resource heavy build. It's up to your threat model if this is a good solution to you


## Gitea
1. copy nginx conf.sample to conf in swag proxy-conf as before and restart swag
2. install gitea and set domain to our domain ,but keep http listen to 3000 so that reverse-proxy going to find it
3. after install create an account which will be admin 
4. edit gitea config file if needed which is  ./gitea_data/gitea/conf/app.ini . few recommended options
	Example settings https://github.com/go-gitea/gitea/blob/main/custom/conf/app.example.ini
	https://docs.gitea.io/en-us/config-cheat-sheet/
```
APP_NAME = The text you want to see on main page

...
;If you using drone you have to add any domain that will be added as webhook to gitea.
[webhook]
ALLOWED_HOST_LIST = drone.yourdomain.com

[other]
SHOW_FOOTER_BRANDING = false
SHOW_FOOTER_VERSION = false
SHOW_FOOTER_TEMPLATE_LOAD_TIME = false
```

5. to setup drone CI follow this https://docs.drone.io/server/provider/gitea/


## Privatebin

if you want to edit config just download https://github.com/PrivateBin/PrivateBin/blob/master/cfg/conf.sample.php config and add file itself as a volume in read only mode
remove hashtag from environment as well to set default config path


